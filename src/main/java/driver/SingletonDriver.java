package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import static constants.Constants.WAIT_TIMEOUT;

public class SingletonDriver {
    public enum Type {CHROME, FIREFOX}

    private static volatile ConcurrentHashMap<Type, WebDriver> drivers = new ConcurrentHashMap<>();
    private static final String nodeURL = "http://192.168.1.181:5004/wd/hub";

    public static WebDriver getDriver(Type type) {
        if (drivers.containsKey(type)) {
            return drivers.get(type);
        }

        WebDriver instance = null;
        if (Type.CHROME.equals(type)) {
            System.setProperty("webdriver.chrome.driver", "C:\\work\\FrameworkTemplate\\src\\main\\resources\\chromedriver.exe");
            DesiredCapabilities capability = DesiredCapabilities.chrome();
            capability.setBrowserName("chrome");
            try {
                instance = new RemoteWebDriver(new URL(nodeURL), capability);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else if (Type.FIREFOX.equals(type)) {
            System.setProperty("webdriver.gecko.driver", "C:\\work\\FrameworkTemplate\\src\\main\\resources\\geckodriver.exe");
            DesiredCapabilities capability = DesiredCapabilities.firefox();
            capability.setBrowserName("firefox");
            try {
                instance = new RemoteWebDriver(new URL(nodeURL), capability);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        instance.manage().window().maximize();
        instance.manage().timeouts().implicitlyWait(WAIT_TIMEOUT, TimeUnit.SECONDS);
        instance.get("https://qa.health.epam.com");
        drivers.put(type, instance);

        return instance;
    }
}
