package parallel.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/parallel/features"},
        glue = "parallel/stepDefs",
        plugin = {"json:target/cucumber-reports/Cucumber.json"}
)

public class TestRunner {
}
